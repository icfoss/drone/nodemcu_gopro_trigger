/*  Firmware for NodeMCU based GoPro Trigger
    Copyright (C) 2018  ICFOSS
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Author:
      Vaisakh Anand (gitlab: vaisakh032)
*/

/****************************************************************************************
 * NodeMCU GoPro Trigger
 * *********************
 * This program will help the user to make an ordinary NodeMCU act as a
 * wifi based GoPro trigger. This work is oriognially based on "Robert Stefanowicz's
 * poroject "DIY GoPro remote " documented at euerdesign electronics.
 * (http://euerdesign.de/2015/12/28/ultralowcost-diy-gopro-remote/)
 *
 * This project aims to use the node MCU as a GoPro Trigger to be used with a
 * Pixhawk Cube flight controller, so that the airborne drone can now wirelessly
 * control the GoPRO camera. The GoPro used here is GoPro HERO 4 Silver. As both
 * the controllers are 3.3v logic based, we dont have to convert the logic.
 *
 * ***********************
 * Circuit Configurations:
 * ***********************
 *
 * -> Led connected to D8 = Denotes whether the NodeMCU is connected with the GoPRO
 * -> Led Connected to D1 = Lights Up when the NodeMCU is sending a command (URl request)
 *                          to the GoPRO.
 * -> Press Button to D2  = Active low logic. When this pin is grounded, it will
 *                          trigger the URL request to the GoPRO. A parallel pin is
 *                          connected to the pixHawk Flight controller so that the
 *                          controller will trigger an active low for triggering the
 *                          camera.
 * _____________________________________________________________________________________
 * Note:
 * The purpose of the project was to use the GoPro for mapping process. Hence I Had to
 * use the GoPro in 7Mp resolution due to huge lense distortion in 12Mp mode. So this
 * camera trigger will automatically set the camera resolution to 7Mp normal in every boot.
 * So, in case if you dont need this function please comment out those lines .
 * ______________________________________________________________________________________
 *
 * Please update your GoPro's wifi ssid and password in this program before uploading it.
 *
 *****************************************************************************************
*/

#include <ESP8266WiFi.h>
/*********************************
* YOUR SETTINGS *
*********************************/

const char* ssid = "hero";          //Your Wifi name (SSID)
const char* password = "12345678";  //Your WiFi password

/*********************************
* DO NOT CHANGE BELOW THIS LINE *
*********************************/
const char* host = "10.5.5.9";
int delshort = 50;

/*
The pixhawk controller is connected to the button pin parallely
so that either the controller or the press button can be used to
trigger the camera. The logic used here is active low. Hence the
button pin has to be grounded to trigger the camera.

 The connect pin is connected with a led which indicates whether the
 NoceMCU is connected with the GoPRO or not.
*/
const int buttonPin = 4;         //Corresponding to D2 in NodeMCU pin out
const int connectPin = 15;       //Corresponding to D8 in NodeMCU pin out
int command_send_led_pin = 5;    //Corresponding to D1 in NodeMCU pin out
int value;
int buttonstate;
int buttonstates;
int camstates;
int checked;
int values;
int valuess;
int command_send_led_pin = 5;    //Corresponding to D1 in NodeMCU pin out

void setup()
{
  Serial.begin(115200);
  delay(100);
  pinMode(buttonPin, INPUT);
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  pinMode(command_send_led_pin, OUTPUT);
  pinMode(connectPin,OUTPUT);
  digitalWrite(connectPin,HIGH);

  /*
   *The below mentioned commands are for sending the default settings
   *required for using the GoPRO for mapping process.
   *Sequence:
   *    1) Put the GoPRO into Photo Mode
   *    2) Set the Resolution to 7Mp Normal
   *    3) Set the Auto Off to Never
  */

  if(!SendCommand("http://10.5.5.9/gp/gpControl/command/mode?p=1")) // to put the GoPro onto Photo Mode
    {
    digitalWrite(connectPin,LOW);
      return;
    }

  if(!SendCommand("http://10.5.5.9/gp/gpControl/setting/17/2")) // To set the 7mp normal mode
    {
    digitalWrite(connectPin,LOW);
      return;
    }

  if(!SendCommand("http://10.5.5.9/gp/gpControl/setting/59/0")) // To set Auto Off to Never
    {
    digitalWrite(connectPin,LOW);
      return;
    }

}



void loop()
{
  values = digitalRead(buttonPin);

  if (buttonstates != values)
  {
  buttonstates = values;
    if (values == 0)       //if button is grounded, i.e it works in active low logic.
    {
      digitalWrite(command_send_led_pin, HIGH);     // Turn ON the command sending indicator led
      if (SendCommand("/gp/gpControl/command/shutter?p=1"))
        digitalWrite(connectPin,HIGH);
      else
        {
          digitalWrite(connectPin,LOW);
          return;
        }
    }
  }

  else
  {
   digitalWrite(command_send_led_pin, LOW); // Turn OFF the command sending indicator led
  }
}


int SendCommand(String url)
{
      WiFiClient client;
      const int httpPort = 80;
      if (!client.connect(host, httpPort))
        {
         Serial.println("connection failed");
         digitalWrite(connectPin,LOW);
         return 0;
        }
      client.print(String("GET ") + url + " HTTP/1.1\r\n" +"Host: " + host + "\r\n" +"Connection: close\r\n\r\n");
      delay(10);
      return 1;
}
