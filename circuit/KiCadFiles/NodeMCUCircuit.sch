EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ESP8266
LIBS:switches
LIBS:NodeMCUCircuit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED ConnectedLED
U 1 1 5C19E6CD
P 6200 4000
F 0 "ConnectedLED" H 6200 4100 50  0000 C CNN
F 1 "LED" H 6200 3900 50  0000 C CNN
F 2 "" H 6200 4000 50  0000 C CNN
F 3 "" H 6200 4000 50  0000 C CNN
	1    6200 4000
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR01
U 1 1 5C19E877
P 6500 4100
F 0 "#PWR01" H 6500 3850 50  0001 C CNN
F 1 "GND" H 6500 3950 50  0000 C CNN
F 2 "" H 6500 4100 50  0000 C CNN
F 3 "" H 6500 4100 50  0000 C CNN
	1    6500 4100
	1    0    0    -1  
$EndComp
$Comp
L LED CommandLED
U 1 1 5C19E897
P 6200 2800
F 0 "CommandLED" H 6200 2900 50  0000 C CNN
F 1 "LED" H 6200 2700 50  0000 C CNN
F 2 "" H 6200 2800 50  0000 C CNN
F 3 "" H 6200 2800 50  0000 C CNN
	1    6200 2800
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR02
U 1 1 5C19E8EC
P 6500 2850
F 0 "#PWR02" H 6500 2600 50  0001 C CNN
F 1 "GND" H 6500 2700 50  0000 C CNN
F 2 "" H 6500 2850 50  0000 C CNN
F 3 "" H 6500 2850 50  0000 C CNN
	1    6500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4000 6050 4000
Wire Wire Line
	5700 3100 5850 3100
Wire Wire Line
	5850 3100 5850 2800
Wire Wire Line
	5850 2800 6050 2800
Wire Wire Line
	6350 2800 6500 2800
Wire Wire Line
	6500 2800 6500 2850
$Comp
L SW_Push Switch
U 1 1 5C19EE4D
P 6250 3450
F 0 "Switch" H 6300 3550 50  0000 L CNN
F 1 "Press to click" H 6250 3390 50  0000 C CNN
F 2 "" H 6250 3650 50  0000 C CNN
F 3 "" H 6250 3650 50  0000 C CNN
	1    6250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3200 6900 3200
Wire Wire Line
	6050 3200 6050 3450
$Comp
L GND #PWR03
U 1 1 5C19EFCE
P 6550 3500
F 0 "#PWR03" H 6550 3250 50  0001 C CNN
F 1 "GND" H 6550 3350 50  0000 C CNN
F 2 "" H 6550 3500 50  0000 C CNN
F 3 "" H 6550 3500 50  0000 C CNN
	1    6550 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3450 6550 3450
Wire Wire Line
	6550 3450 6550 3500
$Comp
L CONN_01X03 P1
U 1 1 5C19EFFD
P 7100 3300
F 0 "P1" H 7100 3500 50  0000 C CNN
F 1 "3Pin Connector to Pixhawk" V 7200 3300 50  0000 C CNN
F 2 "" H 7100 3300 50  0000 C CNN
F 3 "" H 7100 3300 50  0000 C CNN
	1    7100 3300
	1    0    0    -1  
$EndComp
Connection ~ 6050 3200
$Comp
L GND #PWR04
U 1 1 5C19F0A1
P 6800 3900
F 0 "#PWR04" H 6800 3650 50  0001 C CNN
F 1 "GND" H 6800 3750 50  0000 C CNN
F 2 "" H 6800 3900 50  0000 C CNN
F 3 "" H 6800 3900 50  0000 C CNN
	1    6800 3900
	1    0    0    -1  
$EndComp
Text Notes 7450 6900 0    139  ~ 28
Node MCU based GoPRO Trigger
Text Notes 8200 7650 0    60   ~ 0
19th December 2018
Text Notes 7400 7500 0    60   ~ 0
Node MCU based GoPRO Trigger V2.0
Text Notes 6650 3200 0    60   ~ 0
Signal
Text Notes 6750 3300 0    60   ~ 0
VCC
Wire Wire Line
	6350 4000 6500 4000
Wire Wire Line
	6500 4000 6500 4100
$Comp
L NodeMCU1.0(ESP-12E) U1
U 1 1 5C19E67F
P 4900 3700
F 0 "U1" H 4900 4550 60  0000 C CNN
F 1 "NodeMCU1.0(ESP-12E)" H 4900 2850 60  0000 C CNN
F 2 "" H 4300 2850 60  0000 C CNN
F 3 "" H 4300 2850 60  0000 C CNN
	1    4900 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4400 4100 4700
Wire Wire Line
	4100 4700 6700 4700
Wire Wire Line
	6700 4700 6700 3300
Wire Wire Line
	6700 3300 6900 3300
Wire Wire Line
	5700 3600 6100 3600
Wire Wire Line
	6100 3600 6100 3800
Wire Wire Line
	6100 3800 6800 3800
Wire Wire Line
	6800 3400 6800 3900
Wire Wire Line
	6800 3400 6900 3400
Connection ~ 6800 3800
Text Notes 6750 3400 0    60   ~ 0
GND
$EndSCHEMATC
