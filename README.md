NodeMCU Gopro Trigger
==========================

This program will help the user to convert an ordinary NodeMCU act as a
wifi based GoPro trigger. This work is oriognially based on "Robert Stefanowicz's
poroject "DIY GoPro remote " documented at [euerdesign electronics](http://euerdesign.de/2015/12/28/ultralowcost-diy-gopro-remote/)

This project aims to convert the node MCU as a GoPro Trigger to be used with a
Pixhawk Cube flight controller, so that the airborne drone can now wirelessly
control the GoPRO camera. The basic need was to develop a wifi trigger so that
we can use the GoPro for mapping process. Hence I Had to use the GoPro in 7Mp
resolution due to huge lense distortion in 12Mp Wide mode. This camera
trigger will automatically set the camera resolution to 7Mp normal in every boot
and will also set the camera to Photo Mode while swithching off Auto Turn off to never.

The GoPro used here is **GoPro HERO 4 Silver**. As both the flight controller
(pixhawk cube) and the NodeMcu are 3.3v logic based, we dont have to use a logic
converted here.

### Circuit Explanation
  The circuit contains minimal components.

#### List of components:
  - tactile switches (1)
  - Leds (2)
  - male header pins

#### Diagram

![circuit Diagram](/circuit/NodeMCUCircuit.png)

####  Circuit Configurations:

- **Led connected to D8** = Denotes whether the NodeMCU is connected with the GoPRO
- **Led Connected to D1** = Lights Up when the NodeMCU is sending a command (URl
  request)to the GoPRO.
- **Press Button to D2**  = Active low logic. When this pin is grounded, it will
trigger the URL request to the GoPRO. A parallel pin is connected to the pixHawk
Flight controller so that thecontroller will trigger an active low for triggering
the camera.

### How to connect it with Pixhawk Flight controller
Here we have to connect the NodeMCU with Pihxak Cube (or any other Flight controller).
The pixhawk flight controller's power rail will not contain power unless you feed in.
So, it is easier to get your 5v vin from your R/C reciever. The circuit cleary
shows how to do so.

### How to Setup the controller to trigger camera through Mission Planner
You will also have to set up the pin. as well as the logic for the pixHawk to
trigger the camera. For this we will use the [MissionPlanner](http://ardupilot.org/planner/docs/mission-planner-overview.html).
After connecting your drone with the MissionPlanner, you have to follow the
the below said instruction:
- Initial Setup -> Optional Hardware -> Camera Gimball

```
Type = Servo
Shutter = Relay

```
![cameraGimball](/missionPlannerInstructions/cameraGimball.png)

- Config/Tuning -> Extended Tuning

```
Ch7  Opt = Camera Trigger
```
![ExtendedTuning](/missionPlannerInstructions/ExtendedTuning.png)

- Config/Tuning -> Full Parameter List

>I am connecting the camera trigger signal pin to Aux5 pin in my pixHawk Cube
so incase you need to cahnge it i have listed the values for the corresonding pins
below:

Aux Port     |  value
----------   |----------
Pixhawk AUX1 |  51
Pixhawk Aux2 |  52
Pixhawk AUX3 |  53
Pixhawk AUX4 |  54
Pixhawk Aux5 |  55
Pixhawk AUX6 |  111
APM1 Relay   |  50
APM2 A9      |  47

```
CAM_RELAY_ON = 1
CH7_OPT = 9
RELAY_DEFAULT = 0
RELAY_PIN = 54
```
![FullParamters](/missionPlannerInstructions/FullParamters.png)


### GoPro URL's
>In case you need to add additional feature i will attach the GoPro URLS requests
below:

- Controls


```
off: http://10.5.5.9/gp/gpControl/setting/10/0
on: http://10.5.5.9/gp/gpControl/setting/10/1
```
- White Balance:


```
Auto: http://10.5.5.9/gp/gpControl/setting/11/0
3000k: http://10.5.5.9/gp/gpControl/setting/11/1
5500k: http://10.5.5.9/gp/gpControl/setting/11/2
6500k: http://10.5.5.9/gp/gpControl/setting/11/3
Native: http://10.5.5.9/gp/gpControl/setting/11/4
```
- Color:


```
GOPRO: http://10.5.5.9/gp/gpControl/setting/12/0
Flat: http://10.5.5.9/gp/gpControl/setting/12/1
```
- ISO:


```
6400: http://10.5.5.9/gp/gpControl/setting/13/0
1600: http://10.5.5.9/gp/gpControl/setting/13/1
400: http://10.5.5.9/gp/gpControl/setting/13/2
```
- Sharpness:


```
High: http://10.5.5.9/gp/gpControl/setting/14/0
Med: http://10.5.5.9/gp/gpControl/setting/14/1
Low: http://10.5.5.9/gp/gpControl/setting/14/2
```
- EV:


```
Value URL
+2 http://10.5.5.9/gp/gpControl/setting/15/0
+1.5 http://10.5.5.9/gp/gpControl/setting/15/1
+1 http://10.5.5.9/gp/gpControl/setting/15/2
+0.5 http://10.5.5.9/gp/gpControl/setting/15/3
0 http://10.5.5.9/gp/gpControl/setting/15/4
-0.5 http://10.5.5.9/gp/gpControl/setting/15/5
-1 http://10.5.5.9/gp/gpControl/setting/15/6
-1.5 http://10.5.5.9/gp/gpControl/setting/15/7
-2 http://10.5.5.9/gp/gpControl/setting/15/8
```
- Primary modes:


```
Video: http://10.5.5.9/gp/gpControl/command/mode?p=0
Photo: http://10.5.5.9/gp/gpControl/command/mode?p=1
MultiShot: http://10.5.5.9/gp/gpControl/command/mode?p=2
```
- Secondary modes:


```
Video (VIDEO): http://10.5.5.9/gp/gpControl/setting/68/0
TimeLapse Video (VIDEO): http://10.5.5.9/gp/gpControl/setting/68/1
Video + Photo (VIDEO): http://10.5.5.9/gp/gpControl/setting/68/2
Looping (VIDEO): http://10.5.5.9/gp/gpControl/setting/68/3
Single (PHOTO): http://10.5.5.9/gp/gpControl/setting/69/0
Continuous (PHOTO): http://10.5.5.9/gp/gpControl/setting/69/1
Night (PHOTO): http://10.5.5.9/gp/gpControl/setting/69/2
Burst (MultiShot): http://10.5.5.9/gp/gpControl/setting/70/0
Timelapse (MultiShot): http://10.5.5.9/gp/gpControl/setting/70/1
NightLapse (MultiShot): http://10.5.5.9/gp/gpControl/setting/70/2
```
- Power:


```
Power Off: http://10.5.5.9/gp/gpControl/command/system/sleep
```
- Frame Rate:


```
120fps: http://10.5.5.9/gp/gpControl/setting/3/0
90fps: http://10.5.5.9/gp/gpControl/setting/3/3
60fps: http://10.5.5.9/gp/gpControl/setting/3/5
48fps: http://10.5.5.9/gp/gpControl/setting/3/7
30fps: http://10.5.5.9/gp/gpControl/setting/3/8
24fps: http://10.5.5.9/gp/gpControl/setting/3/10
```
- Resolutions:


```
4K: http://10.5.5.9/gp/gpControl/setting/2/1
4K SuperView: http://10.5.5.9/gp/gpControl/setting/2/2
2.7K: http://10.5.5.9/gp/gpControl/setting/2/4
2.7K SuperView: http://10.5.5.9/gp/gpControl/setting/2/5
2.7K 4:3: http://10.5.5.9/gp/gpControl/setting/2/6
1440p: http://10.5.5.9/gp/gpControl/setting/2/7
1080p SuperView: http://10.5.5.9/gp/gpControl/setting/2/8
1080p: http://10.5.5.9/gp/gpControl/setting/2/9
960p: http://10.5.5.9/gp/gpControl/setting/2/10
720p SuperView: http://10.5.5.9/gp/gpControl/setting/2/11
720p: http://10.5.5.9/gp/gpControl/setting/2/12
WVGA: http://10.5.5.9/gp/gpControl/setting/2/13
```
- Exposure time for NightPhoto:


```
Auto: http://10.5.5.9/gp/gpControl/setting/19/0
2: http://10.5.5.9/gp/gpControl/setting/19/1
5: http://10.5.5.9/gp/gpControl/setting/19/2
10: http://10.5.5.9/gp/gpControl/setting/19/3
15: http://10.5.5.9/gp/gpControl/setting/19/4
20: http://10.5.5.9/gp/gpControl/setting/19/5
30: http://10.5.5.9/gp/gpControl/setting/19/6
```
- Exposure time for NightLapse:


```
Auto: http://10.5.5.9/gp/gpControl/setting/31/0
2: http://10.5.5.9/gp/gpControl/setting/31/1
5: http://10.5.5.9/gp/gpControl/setting/31/2
10: http://10.5.5.9/gp/gpControl/setting/31/3
15: http://10.5.5.9/gp/gpControl/setting/31/4
20: http://10.5.5.9/gp/gpControl/setting/31/5
30: http://10.5.5.9/gp/gpControl/setting/31/6
```
- Photo resolution:


```
12MP Wide: http://10.5.5.9/gp/gpControl/setting/17/0
7MP Wide: http://10.5.5.9/gp/gpControl/setting/17/1
7MP Medi: http://10.5.5.9/gp/gpControl/setting/17/2
5MP Wide: http://10.5.5.9/gp/gpControl/setting/17/3
```
- Field Of View:


```
Wide: http://10.5.5.9/gp/gpControl/setting/4/0
Medium: http://10.5.5.9/gp/gpControl/setting/4/1
Narrow: http://10.5.5.9/gp/gpControl/setting/4/2
```
- Low Light:


```
ON: http://10.5.5.9/gp/gpControl/setting/8/1
OFF: http://10.5.5.9/gp/gpControl/setting/8/0
```
- Timelapse Interval (TIMELAPSE MODE on MultiShot):


```
0.5: http://10.5.5.9/gp/gpControl/setting/5/0
1: http://10.5.5.9/gp/gpControl/setting/5/1
2: http://10.5.5.9/gp/gpControl/setting/5/2
5: http://10.5.5.9/gp/gpControl/setting/5/3
10: http://10.5.5.9/gp/gpControl/setting/5/4
30: http://10.5.5.9/gp/gpControl/setting/5/5
60: http://10.5.5.9/gp/gpControl/setting/5/6
```
- Continuous photo rate:


```
3: http://10.5.5.9/gp/gpControl/setting/18/0
5: http://10.5.5.9/gp/gpControl/setting/18/1
10: http://10.5.5.9/gp/gpControl/setting/18/2
```
- Video Looping Duration:


```
Max: http://10.5.5.9/gp/gpControl/setting/6/0
5Min: http://10.5.5.9/gp/gpControl/setting/6/1
20Min: http://10.5.5.9/gp/gpControl/setting/6/2
60Min: http://10.5.5.9/gp/gpControl/setting/6/3
120Min: http://10.5.5.9/gp/gpControl/setting/6/4
```
- Video+Photo Interval:


```
5: http://10.5.5.9/gp/gpControl/setting/7/1
10: http://10.5.5.9/gp/gpControl/setting/7/2
30: http://10.5.5.9/gp/gpControl/setting/7/3
60Min: http://10.5.5.9/gp/gpControl/setting/7/4
```
- Spot Meter:


```
off: http://10.5.5.9/gp/gpControl/setting/9/0
on: http://10.5.5.9/gp/gpControl/setting/9/1
```
- Shutter:


```
Trigger: http://10.5.5.9/gp/gpControl/command/shutter?p=1
Stop (Video/Timelapse): http://10.5.5.9/gp/gpControl/command/shutter?p=0
```
- Streaming:


```
Start Streaming http://10.5.5.9/gp/gpControl/execute?p1=gpStream&c1=start
Restart Streaming http://10.5.5.9/gp/gpControl/execute?p1=gpStream&c1=restart
Stop Streaming http://10.5.5.9/gp/gpControl/execute?p1=gpStream&c1=stop
Video can be streamed by using aplay or ffplay on udp://:8554, connection must be kept
alive using hero4-udp-keep-alive-send.py script.
```
